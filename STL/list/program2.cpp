//capacity:

#include<iostream>
#include<list>

int main(){

	std::list<int> l={10,20,30,40,50};
	std::cout<<l.empty()<<std::endl;		//0->false
	int count;
	while( !l.empty()){			
	count++;
	std::cout<<l.front()<<" ";		//10 20 30 40 50
	
	l.pop_front();
	}
	std::cout<<std::endl;
	
	std::cout<<count<<std::endl;		//5
	
	// creating empty list 
	std::list<int>l2={};
	std::cout<<l.empty()<<std::endl;	//1->true
	}
