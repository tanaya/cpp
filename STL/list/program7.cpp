//emplace_front()

#include<iostream>
#include<list>

int main(){
	std::list<int>l={10,20,30,40,50};
	
	l.emplace_front(5);
			
	std::list<int>::iterator it;
	for(it=l.begin();it != l.end() ;it++){
		std::cout<<*it<<" "<<std::endl;	//5,10,20,30,40,50
		}
		}
	
