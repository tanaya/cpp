//CAPACITY:

#include<iostream>
#include<list>
int main(){

	std::list<int>l={10,20,30,40};
	std::cout<<l.size()<<std::endl;		//4
	
	std::cout<<l.max_size()<<std::endl;		//384307168202282325
	
	//resize()-> 20,30,40 are removed
	l.resize(1);
	
	std::cout<<l.size()<<std::endl;		//1
	std::list<int>::iterator it;
	for(it=l.begin();it != l.end() ;it++){
		std::cout<<*it<<" "<<std::endl;	//10
		}
		
	l.resize(10);
	for(it=l.begin();it != l.end() ;it++){
		std::cout<<*it<<" "<<std::endl;	//10
						//0...
		}
	}
	
	
