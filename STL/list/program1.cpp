// ITERATORS:

#include<iostream>
#include<list>

int main(){
	std::list<int>l={10,20,30,40};
	
	
	//normal iterator
	std::list<int>::iterator it;
	for(it=l.begin();it != l.end() ;it++){
	
	//*it=10;	->  allowed
	
	std::cout<< *it << " ";		//10 20 30 40
	
	}	
	
	
	/*for(it=l.rbegin();it != l.rend() ;it++){	//error:no match for operator=(because it returns reverse iterator )
	std::cout<< *it <<" ";
	}*/
	
	
	//reverse iterator
	std::list<int>::reverse_iterator ri;
	for(ri=l.rbegin() ; ri != l.rend() ; ri++){	//40 30 20 10
	
	//*ri =10;	->allowed
	
	std::cout<< *ri <<" ";
	}
	
	//constant iterator
	std::list<int>::const_iterator ci;
	for(ci = l.cbegin(); ci != l.cend() ; ci++){
		
		//*ci=10;	 error: assignment of read-only location ‘ci.std::_List_const_iterator<int>::operator*()’
		
		std::cout<< *ci <<" ";		//10 20 30 40
		}
	
	
	//constant reverse iterator
	
	std::list<int>::const_reverse_iterator cri;
	for(cri =l.crbegin() ; cri != l.crend() ;cri++){
	std::cout<<*cri<<" ";			//40 30 20 10
	}
		
	
	}
