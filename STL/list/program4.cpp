// ACCES ELEMENTS

#include<iostream>
#include<list>

int main(){
	std::list<int> l={};
	
	std::cout<< l.front()<<std::endl;	//0
	std::cout<< l.back()<<std::endl;	//0
	
	std::list<int> l2={10,20,30,40,50};
	
	std::cout<< l2.front()<<std::endl;	//10
	std::cout<< l2.back()<<std::endl;	//50
	
	//only returns that element
	std::list<int>::iterator it;
	for(it=l2.begin();it != l2.end() ;it++){
		std::cout<<*it<<" "<<std::endl;	//10 20 30 40 50	
		}
	}
	
