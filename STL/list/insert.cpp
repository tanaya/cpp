#include<iostream>
#include<list>

int main(){
	std::list<int>l;
	
	
	//l.insert(2);	-> error
	
	
	std::list<int>::iterator i;
	l.insert(l.begin(),2);
	for(i=l.begin() ;i != l.end() ;i++){
	std::cout<<*i<<" ";			//2
	}
	
	l.insert(l.end(),3);
	for(i=l.begin() ;i != l.end() ;i++){
	std::cout<<*i<<" ";			// 2 3
	}
	
	l.insert(l.begin(),20,30);		// 20 vela 30 print hoto
	for(i=l.begin() ;i != l.end() ;i++){
	std::cout<<*i<<" ";			// 30 30 30 30 ......... 2 3
	}
	
	}
