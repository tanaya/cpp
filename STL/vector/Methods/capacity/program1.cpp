
#include<iostream>
#include<vector>
int main(){
	std::vector<int>v={10,20,30};
	
	std::vector<int>::iterator it;
	for(it=v.begin();it != v.end() ;it++){
	std::cout<<*it<<" ";		// 10 20 30
	}
	
	//size()
	std::cout<< v.size()<<std::endl;	//3
	
	//max_size()
	std::cout<< v.max_size()<<std::endl;	//2305843009213693951
	
	//resize()
	v.resize(5);
	
	for(it=v.begin();it != v.end() ;it++){
	std::cout<<*it<<" ";		// 10 20 30 0 0
	}
	
	//*check it capacity()
	std::cout<< v.capacity()<<std::endl;	//6
	
	//empty
	std::cout<< v.empty()<<std::endl;	//0
	/*
	v.reserve(2);
	for(it=v.begin();it != v.end() ;it++){
	std::cout<<*it<<" ";		// 10 20 30 0 0
	}
	std::cout<< v.capacity()<<std::endl;
	*/
	}
