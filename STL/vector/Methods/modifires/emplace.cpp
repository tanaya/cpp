#include<iostream>
#include<vector>

int main(){
	std::vector<int> l={10,20,30,40,50};
	
	/*emplace_front(..);
	l.emplace_front(5);
	std::vector<int>::iterator it;
	for(it=l.begin();it != l.end() ;it++){
		std::cout<< *it<<" ";		//  5 10 20 30 40 50
		}
		
	*/	
		
		
	//emplace_back(..);
	l.emplace_back(60);
	std::vector<int>::iterator it2;
	for(it2=l.begin();it2 != l.end() ;it2++){
		std::cout<< *it2<<" ";		// 5 10 20 30 40 50 60
	
		}
		
	
	
	
	
	
	/*emplace_front();
	l.emplace_front();				
	std::vector<int>::iterator i;
	for(i=l.begin();i != l.end() ;i++){
		std::cout<< *i<<" ";		// 0 5 10 20 30 40 50 60	
		}	
	
	*/
	
	std::vector<int>::iterator i;
	
	//emplace_back();				
	l.emplace_back();				
	
	for(i=l.begin();i != l.end() ;i++){
		std::cout<< *i<<" ";		//  0 5 10 20 30 40 50 60 0	
		}	
	
	//emplace()
	std::vector<int>l2;
	//l2.emplace_front(2);
	l2.emplace_back(3);
	//l2.emplace(2);		//error
	for(i=l2.begin();i != l2.end() ;i++){
		std::cout<< *i<<" ";		//  2 3	
		}
	
	// emplace la compulsory 2 parameters ahet 1st ha pointer asto tyanantar 2nd parameter copy hoto
	l2.emplace(l2.end(),3);
	for(i=l2.begin();i != l2.end() ;i++){
		std::cout<< *i<<" ";		//  2 3 3	
		}
	}
	
