#include<iostream>
#include<vector>

int main(){
	std::vector<int>v={10,20,30,40,50};
	
	/*
	-- vector does not contains the push_front() method
	push_front();
	v.push_front(5);
	std::vector<int>::iterator it;
	for(it=v.begin();it != v.end() ;it++){
		std::cout<< *it<<" ";		// 5 10 20 30 40 50
		}
		
		
	*/	
		
	//push_back();
	v.push_back(60);
	std::vector<int>::iterator it2;
	for(it2=v.begin();it2 != v.end() ;it2++){
		std::cout<< *it2<<" ";		//5 10 20 30 40 50 60
	
		}
		
	//v.push_back();		//error
	
		
		}
		
