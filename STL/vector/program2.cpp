//vector with user defined class
#include<iostream>
#include<vector>

class Player{
	int JerNo;
	std::string pName;

	public:
		Player(int JerNo,std::string pName){
			this->JerNo=JerNo;
			this->pName=pName;
		}

		void info(){
			std::cout<<"jerno: "<<JerNo<<""<<"Name :"<<pName<<std::endl;
		}
};
int main(){
	Player pOne(18,"Virat");
	Player pTwo(7,"MSD");
	Player pThree(45,"Rohit");

	std::vector<Player> pobj={pOne,pTwo,pThree};
	for(int i=0;i<pobj.size();i++){
		pobj[i].info();
	}
	return 0;
}

