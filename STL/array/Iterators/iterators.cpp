// ITERATORS:
// array ->            template<class T,size_t N > class array
#include<iostream>
#include<array>

int main(){
	std::array<int,4>l={10,20,30,40};
	
	
	//normal iterator
	std::array<int,4>::iterator it;
	for(it=l.begin();it != l.end() ;it++){
	
	//*it=10;	->  allowed
	
	std::cout<< *it << " ";		//10 20 30 40
	
	}	
	
	
	
	
	
	//reverse iterator
	std::array<int,4>::reverse_iterator ri;
	for(ri=l.rbegin() ; ri != l.rend() ; ri++){	//40 30 20 10
	
	//*ri =10;	->allowed
	
	std::cout<< *ri <<" ";
	}
	
	//constant iterator
	std::array<int,4>::const_iterator ci;
	for(ci = l.cbegin(); ci != l.cend() ; ci++){
		
		//*ci=10;	 error: assignment of read-only location ‘ci.std::_List_const_iterator<int>::operator*()’
		
		std::cout<< *ci <<" ";		//10 20 30 40
		}
	
	
	//constant reverse iterator
	
	std::array<int,4>::const_reverse_iterator cri;
	for(cri =l.crbegin() ; cri != l.crend() ;cri++){
	std::cout<<*cri<<" ";			//40 30 20 10
	}
		
	
	}
