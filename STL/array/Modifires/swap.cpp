#include<iostream>
#include<array>

//size of both arrays has to be same 

int main(){
	std::array<int,3>a1={10,20,30};
	std::array<int,3>a2={40,50,60};
	
	a1.swap(a2);
	
	std::array<int,4>::iterator it;
	for(it=a1.begin();it != a1.end() ;it++){
	std::cout<<*it<<" ";		// 40 50 60
	}
	}
